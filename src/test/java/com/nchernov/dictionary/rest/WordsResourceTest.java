package com.nchernov.dictionary.rest;

import com.nchernov.dictionary.entity.Word;
import com.nchernov.dictionary.exceptions.WordNotFoundException;
import com.nchernov.dictionary.services.EntityDAO;
import com.nchernov.dictionary.services.WordsService;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by zugzug on 30.09.15.
 */
public class WordsResourceTest {
    private static EntityDAO<Word> mockDAO = mock(EntityDAO.class);
    private static EntityDAO<Word> mockEmptyDAO = mock(EntityDAO.class);
    private static EntityDAO<Word> singleItemDAO = mock(EntityDAO.class);
    private static EntityDAO<Word> pairItemsDAO = mock(EntityDAO.class);
    private static EntityDAO<Word> listItemsDAO = new EntityDAO<Word>() {
        private List<Word> words = new ArrayList<Word>();
        {
            words.add(new Word(1L, "Chicken"));
            words.add(new Word(2L, "Egg"));
        }
        @Override
        public Word save(Word entity) {
            return entity;
        }

        @Override
        public Word delete(Word entity) {
            words.remove(entity);
            return entity;
        }

        @Override
        public Collection<Word> find(String query) {
            return null;
        }
    };

    static {
        when(mockDAO.find(any(String.class))).thenReturn(Arrays.asList(new Word(1L, "House"), new Word(2L, "MD")));
        when(singleItemDAO.find(any(String.class))).thenReturn(Arrays.asList(new Word(1L, "House")));
        when(pairItemsDAO.find(contains("House"))).thenReturn(Arrays.asList(new Word(1L, "House")));
        when(pairItemsDAO.find(contains("Tree"))).thenReturn(Arrays.asList(new Word(2L, "Tree")));
        when(pairItemsDAO.find(contains("1"))).thenReturn(Arrays.asList(new Word(1L, "House")));
        when(pairItemsDAO.find(contains("2"))).thenReturn(Arrays.asList(new Word(2L, "Tree")));
    }

    @Test(expected = WordNotFoundException.class)
    public void deleteNotPresentWord() throws JAXBException {
        WordsResource wordsResource = new WordsResource(new WordsService(mockEmptyDAO));
        wordsResource.deleteWord(1L);
    }

    @Test(expected = WordNotFoundException.class)
    public void deleteSameWordTwice() throws JAXBException {
        WordsResource wordsResource = new WordsResource(new WordsService(mockEmptyDAO));
        wordsResource.deleteWord(1L);
        wordsResource.deleteWord(1L);
    }
}
