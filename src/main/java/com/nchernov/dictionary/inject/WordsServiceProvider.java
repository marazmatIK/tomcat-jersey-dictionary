package com.nchernov.dictionary.inject;

import com.nchernov.dictionary.services.EntityDAO;
import com.nchernov.dictionary.services.WordsService;
import com.sun.jersey.spi.inject.SingletonTypeInjectableProvider;

import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
/**
 * Created by zugzug on 01.10.15.
 */
@Provider
public class WordsServiceProvider extends SingletonTypeInjectableProvider<Context, WordsService> {
    public WordsServiceProvider(@Context EntityDAO wordEntityDAO) {
        super(WordsService.class, new WordsService(wordEntityDAO));
    }
}
