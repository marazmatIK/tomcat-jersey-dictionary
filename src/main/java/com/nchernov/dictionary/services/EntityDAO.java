package com.nchernov.dictionary.services;

import java.util.Collection;

/**
 * Created by zugzug on 20.09.15.
 */
public interface EntityDAO<T> {
    public T save(T entity);
    public T delete(T entity);
    public Collection<T> find(String query);
}
