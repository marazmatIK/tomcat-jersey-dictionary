package com.nchernov.dictionary.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by zugzug on 21.09.15.
 */
@XmlRootElement(name = "Error")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Error {
    public static final Error NO_ERROR = new Error("Success", "Operation was successfull");
    public static final Error SERIALIZATION_ERROR = new Error("SERIALIZATION_ERROR", "Cannot serialize object body");
    public static final String VALIDATION_ERROR = "VALIDATION_ERROR";
    public static final String DICTIONARY_CONSTRAINT_VIOLATED = "DICTIONARY_CONSTRAINT_VIOLATED";
    public static final String WORD_NOT_FOUND = "Word not found";
    public static final String WORD_NOT_FOUND_BY_ID = "Word not found. Check id.";
    private String code;
    private String message;
    public static final Error ERROR_NO_WORD_FOUND = new Error("NOT_FOUND", WORD_NOT_FOUND);
    public static final Error ERROR_NO_WORD_FOUND_BY_ID = new Error("NOT_FOUND", "Word not found. Check id.");
    public static final String ERROR_CODE = "SERVER_ERROR";
    public static final String ERROR_CODE_SERVER_ERROR = "SERVER_ERROR";
    public static final String ERROR_INCORRECT_PARAMETERS = "INCORRECT_PARAMETERS";


    public Error(){}
    public Error(String code, String message){
        this.code = code;
        this.message = message;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
