package com.nchernov.dictionary.entity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by zugzug on 20.09.15.
 */
@Entity
@Table(name = "word")
@XmlRootElement(name = "Word")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Word {

    public Word() {}
    public Word(Long id, String value) {
        this.id = id;
        this.value = value;
    }

    /** id of the word */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="value")
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int hashCode() {
        int res = 3;
        return res * ((value == null) ? 1 : value.hashCode()) + 17;
    }

    public boolean equals(Object other) {
        if (!(other instanceof Word)) return false;
        Word word = (Word) other;
        if (this.value == null && word.value != null) {
            return false;
        } else if (this.value != null && word.value == null) {
            return false;
        }
        return this.value.equals(word.value);
    }
}
