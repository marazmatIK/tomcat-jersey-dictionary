package com.nchernov.dictionary.exceptions;

import javax.ws.rs.core.Response;

/**
 * Created by zugzug on 20.09.15.
 */
public class DictionaryException extends AppException {
    public DictionaryException(String message) {
        super(message);
    }

    public int getStatus() {
        return Response.Status.BAD_REQUEST.getStatusCode();
    }
}
